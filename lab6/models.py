from django.db import models


class Todo(models.Model):
    title = models.CharField(max_length=27)
    description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
 
class User(models.Model):
    nama = models.CharField(max_length = 100)
    email = models.EmailField(unique=True, db_index=True)
    password = models.CharField(max_length = 16)
# Create your models here.
