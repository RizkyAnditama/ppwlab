from django.urls import re_path, path, include
from django.conf.urls import url
from .views import *
from django.conf import settings
from django.contrib.auth.views import LogoutView
    
urlpatterns = [
        path('lab-6/', index, name='index'),
        path('', index, name='index'),
        url(r'^add_todo/$', add_todo, name='add_todo'),
        path('profile/', profile, name='profile'),
        path('library/', library, name='library'),
        path('import_json/', import_json, name='import_json'),
        path('registration/', registration, name = 'registration'),
        path('register/', register, name='register'),
        path('email-check/', check_email, name = 'check_email'),
        path('subscriber/', subscriber, name='subscriber'),
        path('display-subscriber/', display_subscriber, name = 'display_subscriber'),
        path('', include('social_django.urls', namespace='social')),
        path('logout/', LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL},name='logout'),
        path('update_count/', update_count, name="update_count"),
        ]
