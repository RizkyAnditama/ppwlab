from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Todo
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Lab6UnitTestFirst(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_using_index_func(self):
        found = resolve('/lab-6/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_todo(self):
        # Creating a new activity
        new_activity = Todo.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab 6 ppw yuhuu')
    
        # Retrieving all available activity
        counting_all_available_todo = Todo.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_form_validation_for_blank_items(self):
        form = Todo_Form(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_form_message_input_has_placeholder(self):
        form = Todo_Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('<label for="id_title">Title:</label>', form.as_p())
        self.assertIn('<label for="id_description">Description:</label>', form.as_p())

    def test_lab6_post_fail(self):
        response = Client().post('/lab-6/add_todo/', {'title': '', 'description': 'A'})
        self.assertEqual(response.status_code, 302)

        
class Lab7FunctionalTest(TestCase):

    def setUp(self):
##        chrome_options = Options()
##        chrome_options.add_argument('--dns-prefetch-disable')
##        chrome_options.add_argument('--no-sandbox')        
##        chrome_options.add_argument('--headless')
##        chrome_options.add_argument('disable-gpu')
##        service_log_path = "./chromedriver.log"
##        service_args = ['--verbose']
##        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
##        self.selenium.implicitly_wait(25)   
##        super(Lab7FunctionalTest, self).setUp()
        super(Lab7FunctionalTest, self).setUp()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
    def tearDown(self):
        self.selenium.quit()
        super(Lab7FunctionalTest, self).tearDown()



    def test_check_navigation_content(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile/')

        navbar = selenium.find_element_by_tag_name('header')
        navbar_content = navbar.get_attribute('innerHTML')

        assert "Home" in navbar_content
        assert "Profile" in navbar_content

    def test_check_if_form_use_css(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/lab-6/')

        form = selenium.find_element_by_id('input-list')
        form_css = form.value_of_css_property('margin-top')

        assert "50px" in form_css

    def test_check_if_profile_title_use_css(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile/')

        form = selenium.find_element_by_id('profile_title')
        form_css = form.value_of_css_property('margin-top')

        assert "100px" in form_css
    
    def test_change_theme(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/lab-6/')
        
        tocheck = selenium.find_element_by_class_name('colorbefore')
        tocheck_color = tocheck.value_of_css_property('background-color')
        self.assertEqual(tocheck_color, 'rgba(250, 250, 250, 1)')
        button = selenium.find_element_by_class_name('changebutton').click()
        tocheck = selenium.find_element_by_class_name('colorafter')
        tocheck_color = tocheck.value_of_css_property('background-color')
        self.assertEqual(tocheck_color, 'rgba(100, 149, 237, 1)')

    def test_accordion(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/profile/')

        accordion = selenium.find_element_by_class_name('accordion-title')
        accordion.click()

        accordion_content = selenium.find_element_by_class_name('accordion-content').text

        assert "University Student of Fasilkom UI" in accordion_content
        
# Create your tests here.

class Lab6UnitTestSecond(TestCase):
    
    def test_lab_6_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_profile_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)
