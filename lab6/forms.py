from django import forms
from .models import *

class Todo_Form(forms.Form):
    attrs = {
        'class' : 'form-control'
        }

    title = forms.CharField(label='Title', required=True, max_length=27, widget=forms.TextInput(attrs=attrs))
    description = forms.CharField(label='Description', required=True, widget=forms.Textarea(attrs=attrs))

    class Meta:
        model = Todo

class Registration_Form(forms.Form):
    attrs = {
        'class' : 'form-control'
        }
    nama = forms.CharField(label = 'Nama', required = True, widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label = 'E-mail', required = True, widget=forms.EmailInput(attrs=attrs))
    password = forms.CharField(label = 'Kata Sandi', required = False, max_length = 32, widget = forms.PasswordInput(attrs=attrs))

    class Meta:
        model = User
