import requests
from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .forms import Todo_Form, Registration_Form
from .models import Todo, User
from django.views.decorators.csrf import csrf_exempt

todo = []
response = {}
def index(request):
    html = 'lab6.html'
    if (request.user.is_authenticated):
        todo = Todo.objects.all()
        response['todo'] = todo
        response['todo_form'] = Todo_Form
    return render(request, html, response)

def registration(request):
    html = 'registration.html'
    response['registration_form'] = Registration_Form
    return render(request, html, response)

def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['title'] = request.POST['title']
        response['description'] = request.POST['description']
        todo = Todo(title=response['title'],description=response['description'])
        todo.save()
        return HttpResponseRedirect('/lab-6/')
    else:
        return HttpResponseRedirect('/lab-6/')

def profile(request):
    return render(request, 'profile.html', {})

def library(request):
    return render(request, 'library.html')

def import_json(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    req = requests.get(url)
    resp = req.json()

    return JsonResponse(resp)

def display_subscriber(request):
    subscribers = User.objects.all().values('nama', 'email')
    subscribers_list = list(subscribers)
    return JsonResponse(subscribers_list, safe = False)
    

@csrf_exempt
def check_email(request):
    email_input = request.POST['email']
    list_email = User.objects.all()
    pesan = "Email tersebut telah terdaftar, mohon gunakan email lain"
    for email_to_check in list_email:
        if email_to_check.email == email_input:
            return JsonResponse({'message' : pesan})
    return JsonResponse({'message': 'Email belum terdaftar, silahkan lanjutkan'})

@csrf_exempt
def register(request):
    nama = request.POST['nama']
    email = request.POST['email']
    password = request.POST['password']
    User(nama=nama, email=email, password=password).save()
    return JsonResponse({'message': 'Data anda berhasil disimpan'})

def subscriber(request):
    return render(request, 'subscriber.html')

@csrf_exempt
def update_count(request):
    nambah = request.POST['nambah']
    if nambah:
        count = request.session.get('count', 0)
        request.session['count'] = count + 1
    else:
        count = request.session.get('count', 0)
        request.session['count'] = count - 1
    return JsonResponse({'count': request.session['count']})
# Create your views here.
